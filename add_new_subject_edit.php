<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Add New Subject</title>


  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12 ">
                <h2 class="content-header btn gradient-blue-grey-blue white shadow-big-navbar">Add new subject</h2>
              </div>
            </div>
            <div class="row">

              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">

                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form method="POST">
                        <div class="form-row">
                          <div class="col-7" style="margin-right: 10px">
                            <input type="text" id="subjectName" class="form-control" placeholder="Enter subject name" required="required" autofocus="autofocus">
                          </div>
                          <div class="col" style="margin-right: 10px">
                            <select class="browser-default custom-select mb-4" id="selectRank">
                              <option value="-1" selected>Choose Rank</option>
                            </select>
                          </div>
                          <div class="col">
                            <input type="button" class="btn btn-primary btn-block" onClick="validateFields()" value="Update" id="update">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                <!--Confirmation Modal-->


              </div>
            </div>
        </div>
        </section>
        <!-- Sticky Footer -->
      </div>
    </div>
  </div>
  <?php include('footer.php'); ?>

  </div>


  <script>
    function getRank() {
      return $("#selectRank option:selected").val();
    }

    function validateFields() {
      if ($("#subjectName").val().trim() === "") {
        showAlertDialog("Suject Name is cannot be blank");
      } else if (getRank() == -1) {
        alert('Please choose correct rank');

      } else {
        submitDetails();
      }
    }

    function submitDetails() {

      var params = {
        coaching_id: getCoachingId(),
        subject_id: localStorage.getItem("subjectId"),
        name: $("#subjectName").val().trim(),
        rank: getRank()
      };
      $.ajax({
        type: "PUT",
        url: BASE_URL + "/coaching/subject/?coaching_id=" + getCoachingId() + "&subject_material_id=" + localStorage.getItem("studyMaterialId"),
        dataType: 'json',
        data: params,
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          showSuccessDialog("Subject edited!");
          window.location.href = "add_new_subject.php";
        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText.error);
        }
      });
    }

    function getSubjectDetail() {
      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/subject/?coaching_id=" + getCoachingId() + "&subject_id=" + localStorage.getItem("subjectId"),
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          $("#selectRank").val(resp.rank);
          // $("#selectRank").prop("disabled", true);
          $("#subjectName").val(resp.name);
        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText.error);
        }
      });
    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }


    $(document).ready(function() {
      ifNotLogin("login.php");

      var subjectRanks = "";
      for (let i = 1; i <= 250; i++) {
        subjectRanks += '<option value=' + i + '>' + i + '</option>';
      }
      $("#selectRank").append(subjectRanks);

      getSubjectDetail();

    });
  </script>
  </body>

</html>