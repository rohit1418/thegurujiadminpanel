<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Paper-wise Result Report</title>
  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-lg-10 offset-lg-1">
                <h2 class="content-header  btn gradient-blue-grey-blue white shadow-big-navbar">Paper-wise result report pdf</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-lg-10 offset-lg-1">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0"> Paper-wise report</h4>
                    </div>
                  </div>
                  <div class="card-body">


                    <form>
                      <div class="form-group col-md-8">
                        <label for="subject">Select Paper</label>
                        <select class="browser-default custom-select mb-4" id="subject">
                          <option value="0" selected>Choose option</option>
                        </select>
                      </div>
                      <div class="form-group col-md-8">
                        <label for="enterPaperCode">Enter Paper Code</label>
                        <div class="form-label-group">
                          <input type="text" id="enterPaperCode" class="form-control" placeholder="Enter Paper Code" required="required" autofocus="autofocus">

                        </div>
                      </div>

                      <a class="btn btn-primary btn-block col-md-4 offset-md-2" href="index.html">Generate</a>
                    </form>

                  </div>
                </div>
              </div>

            </div>
        </div>
        </section>
      </div>
    </div>
  </div>
  </div>
  </div>
  <!-- /.container-fluid -->

  <!-- Sticky Footer -->
  <?php include('footer.php'); ?>
  </body>

</html>