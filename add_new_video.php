<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Add New Video</title>


  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12">
                <h2 class="content-header btn gradient-blue-grey-blue white shadow-big-navbar">Add new video</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0">Add new video</h4>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form>
                        <div class="form-group row" style="
    margin-bottom: -5px;
">
                          <div class="col-md-6">
                            <label>Topic Title</label>
                            <select class="browser-default custom-select mb-4" id="selectTopicTitle">
                              <option value="0" id="-1" selected>Choose option</option>
                            </select>
                          </div>

                          <div class="col-md-6">
                            <label for="videoTitle">Video Title</label>
                            <div class="form-label-group">
                              <input type="text" id="videoTitle" class="form-control" placeholder="Video Title" required="required" autofocus="autofocus">

                            </div>
                          </div>

                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label>Rank</label>
                            <select class="browser-default custom-select mb-4" id="selectTopicRank">
                              <option value="-1" selected>Choose option</option>
                            </select>
                          </div>

                          <div class="col-md-6">
                            <label for="videoLink">Video Link</label>
                            <div class="form-label-group">
                              <input type="text" id="videoLink" class="form-control" placeholder="Video Link" required="required" autofocus="autofocus">

                            </div>
                          </div>
                        </div>

                        <div class="form-group row">


                          <div class="col-md-6">
                            <label>Access</label>
                            <div class="col-md-6">
                              <div class="form-check-inline">
                                <label class="form-check-label" for="public">
                                  <input type="radio" class="form-check-input" id="public" name="status" value="public" checked>Public
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="private">
                                  <input type="radio" class="form-check-input" id="private" name="status" value="private">Private
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="videoDescription">Video Description</label>
                            <textarea class="form-control" id="videoDescription" rows="4" placeholder="Description..."></textarea>
                          </div>

                        </div>
                    </div>

                    <input type="button" class="btn btn-primary btn-block col-sm-6 offset-sm-3 col-lg-6 offset-lg-3 col-md-6 offset-md-3" name="submit" id="submit" onClick="validateFields()" value="Submit">
                    </form>

                  </div>
                </div>
              </div>

              <div style="margin-top: 1rem"></div>


              </div>
            </div>
        </div>
        </section>
        <!-- Sticky Footer -->
      </div>
    </div>
  </div>
  <?php include('footer.php'); ?>

  </div>
    <script>
      function getVideoTopicList() {


        $.ajax({
          type: "GET",
          url: BASE_URL + "/coaching/videotopiclist/?coaching_id=" + getCoachingId(),
          dataType: 'json',
          data: '{}',
          async: false,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', "Bearer " + getToken());
          },
          success: function(resp) {
            var topicTitle = "";
            for (let i = 0; i < resp.length; i++) {
              topicTitle += '<option value=' + resp[i].id + '>' + resp[i].title + '</option>';
            }
            $("#selectTopicTitle").append(topicTitle);

            if (localStorage.getItem("videoTopicTitleId") != "0") {
              $("#selectTopicTitle").val(localStorage.getItem("videoTopicTitleId"));
            }

          },
          error: function(xhr, ajaxOptions, thrownError) {
            showAlertDialog(xhr.responseText + "\n" + xhr.status + "\n" + thrownError);
          }
        });
      }

      function getTopicTitleId() {
        return $("#selectTopicTitle option:selected").val();
      }

      function getRank() {
        return $("#selectTopicRank option:selected").val();
      }

      function validateFields() {
        if (getTopicTitleId() == -1) {
          showAlertDialog('Please choose correct Topic Title');

        } else if ($("#videoTitle").val().trim() === "") {
          showAlertDialog("Video Title can not be blank");

        } else if ($("#videoDescription").val().trim() === "") {
          showAlertDialog("Video Description can not be blank");

        } else if ($("#videoLink").val().trim() === "") {
          showAlertDialog("Video Link can not be blank");

        } else if (getRank() == -1) {
          showAlertDialog('Please choose correct rank');

        } else {
          submitDetail();
        }
      }

      function getStatus() {
        return $('input[name=status]:checked').val();
      }


      function submitDetail() {
        $.ajax({
          type: "POST",
          url: BASE_URL + "/coaching/videovacancylist/",
          dataType: 'json',
          data: {
            videovacancymodel_id: getTopicTitleId(),
            rank: getRank(),
            video_link: $("#videoLink").val().trim(),
            title: $("#videoTitle").val().trim(),
            description: $("#videoDescription").val().trim(),
            coaching_id: getCoachingId(),
            status: getStatus()
          },
          async: false,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', "Bearer " + getToken());
          },
          success: function(resp) {
            showSuccessDialog('New video added!');
            window.location.href = "add_new_video_list.php"

          },
          error: function(xhr, ajaxOptions, thrownError) {
            showAlertDialog(xhr.responseText.error);
          }
        });
      }

      function ifNotLogin(loginPage) {
        if (!localStorage.getItem("access_token")) {
          window.location.href = loginPage;
        }
      }

      $(document).ready(function() {
        ifNotLogin("login.php");

        getVideoTopicList();

        var videoRanks = "";
        for (let i = 1; i <= 250; i++) {
          videoRanks += '<option value=' + i + '>' + i + '</option>';
        }
        $("#selectTopicRank").append(videoRanks);
      });
    </script>
    </body>

</html>