<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Test Paper</title>

</head>

<?php include('header.php'); ?>

<div class="main-panel">
  <div class="main-content">
    <div class="content-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-lg-10 offset-lg-1">
            <div style="display: inline;">
              <div class="card">
                <!--Get Subject List-->
                <div class="card-header">
                  <div class="box" style="float: right;">
                    <p class="pstyle">Add new test paper</p>
                    <p><a href="add_new_test_paper.php" class="divLink" style="text-decoration: none"></a></p>
                  </div>
                  <div class="box">
                    <p class="pstyle">Add new vacancy</p>
                    <p><a href="add_new_vacancy.php" class="divLink" style="text-decoration: none"></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <section id="grid-option">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <!--Get Subject List-->
                <div class="card-header">
                  <div class="pull-left">
                    <h4 class="card-title  btn gradient-blue-grey-blue white shadow-big-navbar">Vacancy List</h4>
                  </div>
                  <div class="pull-right">
                    <input type="text" class="searchBar" id="myInput" placeholder="Search for names.." title="Type in a name" />
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-sm" id="dataTable" width="100%" cellspacing="0">
                      <thead class="thead-dark">
                        <tr>
                          <th>S No.</th>
                          <th>Vacancy Title</th>
                          <th>Date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="tBody"></tbody>
                    </table>
                  </div>
                </div>
              </div>

              <!--Confirmation Modal-->


            </div>
          </div>
      </div>
      </section>
      <!-- Sticky Footer -->
    </div>
  </div>
</div>
<?php include('footer.php'); ?>

</div>
<div class="modal fade" id="confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
        Are you sure, you want to delete?
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" id="delete">Delete</button>
        <button type="button" data-dismiss="modal" class="btn btn-sm">Cancel</button>
      </div>
    </div>
  </div>
</div>
<script>
  var isActionClicked = false;

  localStorage.setItem("vacancy_title_id", "0");
  localStorage.setItem("vacancyTitleId", "0");


  function showPaperList(el) {
    if (!isActionClicked) {
      localStorage.setItem("vacancy_title_id", el.id);
      window.location.href = "add_new_test_paper_list.php";
    }
    isActionClicked = false;
  }

  function editItem(el) {
    isActionClicked = true;
    localStorage.setItem("vacancyTitleId", el.id);
    window.location.href = "add_new_vacancy_edit.php";
  }

  function deleteItem(el) {
    isActionClicked = true;

    $('#confirm').modal({
        backdrop: 'static',
        keyboard: false
      })
      .on('click', '#delete', function() {
        $.ajax({
          type: "DELETE",
          url: BASE_URL + "/coaching/vacancy/?coaching_id=" + getCoachingId() + "&vacancy_id=" + el.id,
          dataType: 'json',
          data: '{}',
          async: false,
          beforeSend: function(xhr) {
            // $('#loading-image').hide();
            xhr.setRequestHeader('Authorization', "Bearer " + getToken());
          },
          success: function(resp) {
            getVacancyList();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            // $('#loading-image').hide();
            // $('#studyMaterialList').show();
            showAlertDialog(xhr.responseText.error);
          }
        });
      });
  }

  function getVacancyList() {
    var query = $("#myInput").val();
    if (query === undefined) {
      query = "";
    } else {
      query = query.trim();
    }
    $.ajax({
      type: "GET",
      url: BASE_URL + "/coaching/coachingvacancylist/?coaching_id=" + getCoachingId() + "&search_type=name" + "&query=" + query,
      dataType: 'json',
      data: '{}',
      async: false,
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Authorization', "Bearer " + getToken());
      },
      success: function(resp) {
        $("#tBody").empty();
        var trHTML = '';
        for (var i = 0; i < resp.length; i++) {
          trHTML +=
            '<tr class="table-row" data-href="add_new_video_list.php" id="' + resp[i].id + '" onclick="showPaperList(this);"' + '>' + '<td>' +
            (i + 1) +
            '</td><td>' +
            resp[i].name +
            '</td><td>' +
            resp[i].created_at__date +
            '</td><td>' +
            '<input type="button" value="Edit" class="btn btn-link" style="color:blue; text-decoration:none;" id="' + resp[i].id + '" onclick="editItem(this);"' + '>' +
            '<input type="button" value="Delete" name="remove_levels" data-toggle="modal" data-target="#confirm"  class="btn btn-link" style="color:red; text-decoration:none;" id="' + resp[i].id + '" onclick="deleteItem(this);"' + '>' +
            '</td></tr>';
        }
        $('#tBody').append(trHTML);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        showAlertDialog(xhr.responseText.error);
      }
    });

  }

  function ifNotLogin(loginPage) {
    if (!localStorage.getItem("access_token")) {
      window.location.href = loginPage;
    }
  }

  $(document).ready(function() {

    ifNotLogin("login.php");

    function delay(callback, ms) {
      var timer = 0;
      return function() {
        var context = this,
          args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
          callback.apply(context, args);
        }, ms || 0);
      };
    }


    $('#myInput').keyup(delay(function(e) {
      getVacancyList();
    }, 500));
    getVacancyList();
  });
</script>

</body>

</html>