<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Edit Paper Part</title>

  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12 ">
                <h2 class="content-header  btn gradient-blue-grey-blue white shadow-big-navbar">Add Test Paper</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0">Edit Paper Part</h4>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="selectTestPaper">Select Test Paper</label>
                            <div class="form-label-group">
                              <select class="custom-select custom-select-sm" id="selectTestPaper">
                                <option value="-1" selected>Choose Option</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="testPaperPartTitle">Test Paper Part Title</label>
                            <div class="form-label-group">
                              <input type="text" class="form-control form-control-sm" id="testPaperPartTitle" placeholder="Test Paper Part Title">
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="time">Time</label>
                            <input type="text" class="form-control form-control-sm" id="time" placeholder="Time in minutes">
                          </div>
                          <div class="col-md-6">
                            <label for="noOfQuestions">No. of Questions</label>

                            <input type="text" class="form-control form-control-sm" id="noOfQuestions" placeholder="Total Questions">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="totalMarks">Total Marks</label>
                            <input type="text" class="form-control form-control-sm" id="totalMarks" placeholder="Total Marks">
                          </div>
                          <div class="col-md-6">
                            <label for="perQuestionScore">Per Question Score</label>

                            <input type="text" class="form-control form-control-sm" id="perQuestionScore" placeholder="Per Question Score">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="vacancy">Rank</label>

                            <select class="custom-select custom-select-sm" id="selectRank">
                              <option value="-1" selected>Choose Option</option>
                            </select>
                          </div>
                          <div class="col-md-6">
                            <label for="instructions">Instructions</label>

                            <textarea class="form-control form-control-sm" id="instructions" placeholder="instructions..."></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label class="custom-file-label" for="inputGroupFile01" style="margin-left: 15px;" value="Choose file" id=filelabel></label>
                            <input type="file" class="custom-file-input form-control form-control-sm" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="file" onchange="onChangeFile();" accept=".xlsx, .xls, .csv">

                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-xs-10">
                            <input type="button" class="btn btn-primary" onClick="validateFields()" value="Update" id="update">
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>
                </div>
              </div>
          </section>
        </div>
      </div>

      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <?php include('footer.php'); ?>
      <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>


      <script>
        var paper_exists = false;
        var testModelId = "";
        var paperSelected = 0;

        function getRank() {
          return $("#selectRank option:selected").val();
        }

        function getTestPaperTitleId() {
          // return  $("#selectTestPaper option:selected").val();
          return localStorage.getItem("testPaperTitleId");
        }

        function validateFields() {
          if (getTestPaperTitleId() == -1) {
            showAlertDialog('Please choose correct Test Paper Title');

          } else if ($("#testPaperPartTitle").val().trim() === "") {
            showAlertDialog("Test Paper Part Title can not be blank");

          } else if ($("#time").val().trim() === "") {
            showAlertDialog("Time can not be blank");

          } else if ($("#noOfQuestions").val().trim() === "") {
            showAlertDialog("No. of Question can not be blank");

          } else if ($("#totalMarks").val().trim() === "") {
            showAlertDialog("Total Marks can not be blank");

          } else if ($("#perQuestionScore").val().trim() === "") {
            showAlertDialog("Per Question Score can not be blank");

          } else if (getRank() == -1) {
            showAlertDialog('Please choose correct rank');

          } else {
            submitDetail();
          }
        }

        function onChangeFile() {
          document.getElementById('filelabel').innerHTML = $("#inputGroupFile01").val().substring(12, 80);
          paperSelected = 1;
        }

        function submitDetail() {

          var formData = new FormData();

          if (paperSelected == 1 || paper_exists === false) {
            formData.append("name", $("#testPaperPartTitle").val().trim());
            formData.append("terms", $("#instructions").val().trim());
            formData.append("no_of_questions", $("#noOfQuestions").val().trim());
            formData.append("score", $("#totalMarks").val().trim());
            formData.append("rank", getRank());
            formData.append("file", $("#inputGroupFile01")[0].files[0]);
            formData.append("per_question_score", $("#perQuestionScore").val().trim());
            formData.append("minutes", $("#time").val().trim());
            formData.append("vacancymodel_id", getTestPaperTitleId());
            formData.append("coaching_id", getCoachingId());
            formData.append("testmodel_id", testModelId);
          } else {
            formData.append("name", $("#testPaperPartTitle").val().trim());
            formData.append("terms", $("#instructions").val().trim());
            formData.append("no_of_questions", $("#noOfQuestions").val().trim());
            formData.append("score", $("#totalMarks").val().trim());
            formData.append("rank", getRank());
            formData.append("per_question_score", $("#perQuestionScore").val().trim());
            formData.append("minutes", $("#time").val().trim());
            formData.append("vacancymodel_id", getTestPaperTitleId());
            formData.append("coaching_id", getCoachingId());
            formData.append("testmodel_id", testModelId);
          }

          var file = $("#inputGroupFile01").val();
          var ext = (function(file, lio) {
            return lio === -1 ? undefined : file.substring(lio + 1);
          })(file, file.lastIndexOf("."));

          $.ajax({
            type: "PUT",
            url: BASE_URL + "/coaching/testmodel/",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('Authorization', 'Bearer ' + getToken());
            },
            success: function(resp) {
              debugger;
              showSuccessDialog('Successfully Submitted!');
              window.location.href = "add_new_test_paper_part_list.php";
            },
            error: function(xhr, ajaxOptions, thrownError) {

              console.log(xhr.responseText);
              var data = xhr.responseText;
              var jsonResponse = JSON.parse(data);
              showAlertDialog(jsonResponse.error);
            }
          });
        }

        function getTestPaperList() {
          $.ajax({
            type: "GET",
            url: BASE_URL + "/coaching/coachingvacancymodellist/?coaching_id=" + getCoachingId() + "&vacancy_id=" + localStorage.getItem("vacancy_title_id"),
            dataType: 'json',
            data: '{}',
            async: false,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('Authorization', "Bearer " + getToken());
            },
            success: function(resp) {
              var testPaperTitle = "";
              for (let i = 0; i < resp.length; i++) {
                testPaperTitle += '<option value=' + resp[i].id + '>' + resp[i].name + '</option>';
              }
              $("#selectTestPaper").append(testPaperTitle);

              if (localStorage.getItem("testPaperTitleId")) {

                $("#selectTestPaper").val(localStorage.getItem("testPaperTitleId"));
              }
            },
            error: function(xhr, ajaxOptions, thrownError) {
              showAlertDialog(xhr.responseText.error);
            }
          });
        }

        function getTestPaperPartDetail() {
          $.ajax({
            type: "GET",
            url: BASE_URL + "/coaching/testmodel/?coaching_id=" + getCoachingId() + "&testmodel_id=" + localStorage.getItem("tespPaperPartId"),
            dataType: 'json',
            data: '{}',
            async: false,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('Authorization', "Bearer " + getToken());
            },
            success: function(resp) {
              testModelId = resp.id;
              $("#testPaperPartTitle").val(resp.name);
              $("#time").val(resp.minutes);
              $("#noOfQuestions").val(resp.no_of_questions);
              $("#totalMarks").val(resp.score);
              $("#perQuestionScore").val(resp.per_question_score);
              $("#instructions").val(resp.terms);
              $("#selectRank").val(resp.rank);

              if (resp.paper_exists === true) {
                paper_exists = true;
                document.getElementById('filelabel').innerHTML = "File already exist! Upload another file.";
              } else {
                paper_exists = false;
                document.getElementById('filelabel').innerHTML = "Upload xlsx file!";

              }
            },
            error: function(xhr, ajaxOptions, thrownError) {
              showAlertDialog(xhr.responseText.error);
            }
          });
        }


        function ifNotLogin(loginPage) {
          if (!localStorage.getItem("access_token")) {
            window.location.href = loginPage;
          }
        }

        $(document).ready(function() {
          ifNotLogin("login.php");

          var paperRanks = "";
          for (let i = 1; i <= 250; i++) {
            paperRanks += '<option value=' + i + '>' + i + '</option>';
          }
          $("#selectRank").append(paperRanks);
          getTestPaperList();
          getTestPaperPartDetail();
        });
      </script>

      </body>

</html>