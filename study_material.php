<!DOCTYPE html>
<html lang="en" class="loading">

<!-- Mirrored from pixinvent.com/demo/convex-bootstrap-admin-dashboard-template/demo-3/basic-forms.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Oct 2018 10:54:14 GMT -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Convex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, Convex admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>Study Material</title>

  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-lg-10 offset-lg-1">
              <div style="display: inline;">
                <div class="card">
                  <div class="card-header">
                    <div class="box" style="float: right;">
                      <p class="pstyle">Add new subject</p>
                      <p><a href="add_new_subject.php" class="divLink" style="text-decoration: none"></a></p>
                    </div>
                    <div class="box ">
                      <p class="pstyle">Add new study material</p>
                      <p><a href="add_new_study_material.php" onclick="addNewStudyMaterial();" class="divLink" style="text-decoration: none"></a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--Bootstrap Grid options Starts-->
          <section id="grid-option">
            <div class="row">
              <div class="col-md-12 ">
                <div class="card">
                  <div class="card-header">
                    <div class="card-header">
                      <div class="pull-left">
                        <h4 class="card-title btn gradient-blue-grey-blue white shadow-big-navbar">Study Material</h4>
                      </div>
                      <div class=" pull-right">
                        <input type="text" class="searchBar" id="myInput" placeholder="Search for names.." title="Type in a name" />
                      </div>
                    </div>
                  </div>
                  <div class="card-body collpase show">
                    <div class="card-block card-dashboard">

                      <table class="table table-striped table-bordered table-sm" id="dataTable" width="100%" cellspacing="0">
                        <thead class="thead-dark">
                          <tr>
                            <th>S No.</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="tBody"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </section>
          <!-- // Basic form layout section end -->
        </div>
      </div>
    </div>

    <?php include('footer.php'); ?>
    <div class="modal fade" id="confirm">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <!-- Modal body -->
          <div class="modal-body">
            Are you sure, you want to delete?
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" id="delete">Delete</button>
            <button type="button" data-dismiss="modal" class="btn btn-sm">Cancel</button>
          </div>
        </div>
      </div>
    </div>


    <script>
      var isActionClicked = false;

      function addNewStudyMaterial() {
        localStorage.setItem("studyMaterialId", "0");
      }

      function showStudyMaterialList(el) {
        if (!isActionClicked) {
          localStorage.setItem("studyMaterialId", el.id);
          window.location.href = "add_new_study_material_edit.php";
        }
        isActionClicked = false;
      }

      function deleteItem(el) {
        isActionClicked = true;
        $('#confirm').modal({
            backdrop: 'static',
            keyboard: false
          })
          .on('click', '#delete', function() {
            $.ajax({
              type: "DELETE",
              url: BASE_URL + "/coaching/studymaterial/?coaching_id=" + getCoachingId() + "&subject_material_id=" + el.id,
              dataType: 'json',
              data: '{}',
              async: false,
              beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', "Bearer " + getToken());
              },
              success: function(resp) {
                getStudyMaterialList();
              },
              error: function(xhr, ajaxOptions, thrownError) {
                showAlertDialog(xhr.responseText.error);
              }
            });
          });

      }

      function getStudyMaterialList() {
        var query = $("#myInput").val();
        if (query === undefined) {
          query = "";
        } else {
          query = query.trim();
        }

        $.ajax({
          type: "GET",
          url: BASE_URL + "/coaching/studymateriallist/?coaching_id=" + getCoachingId() + "&search_type=name" + "&query=" + query,
          dataType: 'json',
          data: '{}',
          async: false,
          beforeSend: function(xhr) {
            $('#loading-image').hide();
            xhr.setRequestHeader('Authorization', "Bearer " + getToken());
          },
          success: function(resp) {
            $("#tBody").empty();
            var trHTML = '';
            for (var i = 0; i < resp.length; i++) {
              trHTML +=
                '<tr class="table-row" data-href="add_new_student_material.php" id="' + resp[i].id + '" onclick="showStudyMaterialList(this);"' + '>' + '<td>' +
                (i + 1) +
                '</td><td>' +
                resp[i].title +
                '</td><td>' +
                resp[i].created_at__date +
                '</td><td>' +
                '<input type="button" value="Delete" name="remove_levels" data-toggle="modal" data-target="#confirm" class="btn btn-link" style="color:red; text-decoration:none;" id="' + resp[i].id + '" onclick="deleteItem(this);"' + '>' +
                '</td></tr>';
            }
            $('#tBody').append(trHTML);
          },
          complete: function() {
            // $('#loading-image').hide();
            // $('#studyMaterialList').show();
          },
          error: function(xhr, ajaxOptions, thrownError) {
            // $('#loading-image').hide();
            // $('#studyMaterialList').show();
            // showAlertDialog(xhr.responseText.error);
          }
        });

      }

      function ifNotLogin(loginPage) {
        if (!localStorage.getItem("access_token")) {
          window.location.href = loginPage;
        }
      }

      // $('#loading-image').show();
      // $('#studyMaterialList').hide();
      $(document).ready(function() {
        ifNotLogin("login.php");

        function delay(callback, ms) {
          var timer = 0;
          return function() {
            var context = this,
              args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function() {
              callback.apply(context, args);
            }, ms || 0);
          };
        }


        $('#myInput').keyup(delay(function(e) {
          getStudyMaterialList();
        }, 500));
        getStudyMaterialList();
      });
    </script>


    </body>

</html>