const BASE_URL = "http://guru.buildroots.in";

function getCoachingId(){
    return localStorage.getItem("coachings_id");
}

function getToken(){
    token = localStorage.getItem("access_token");
    return token;
}

function getId(){
    return localStorage.getItem("id");
}

function getName(){
    return localStorage.getItem("name");
}

function getEmail(){
    return localStorage.getItem("email");
}

function showAlertDialog(param){
    alert(param);
}

function showSuccessDialog(param){
    alert(param);
}

function showErrorDialog(param){
    alert(param);
}




