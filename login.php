<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login</title>
    <link rel="apple-touch-icon" sizes="60x60" href="app-assets/img/ico/apple-icon-60.html">
    <link rel="apple-touch-icon" sizes="76x76" href="app-assets/img/ico/apple-icon-76.html">
    <link rel="apple-touch-icon" sizes="120x120" href="app-assets/img/ico/apple-icon-120.html">
    <link rel="apple-touch-icon" sizes="152x152" href="app-assets/img/ico/apple-icon-152.html">
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/demo/convex-bootstrap-admin-dashboard-template/app-assets/img/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="app-assets/img/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/prism.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">

</head>

<body data-col="1-column" class=" 1-column  blank-page blank-page">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="wrapper">
        <section id="login">
            <div class="container-fluid">
                <div class="row full-height-vh">
                    <div class="col-12 d-flex align-items-center justify-content-center gradient-aqua-marine">
                        <div class="card px-4 py-2 box-shadow-2 width-400">
                            <div class="card-header text-center">
                                <img src="app-assets/img/logos/logo-color-big.png" alt="company-logo" class="mb-3" width="80">
                                <h4 class="text-uppercase text-bold-400 grey darken-1">Login</h4>
                            </div>
                            <div class="card-body">
                                <div class="card-block">
                                    <form method="POST">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control form-control-lg" id="inputUsername" placeholder="Username" autofocus="autofocus">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="password" class="form-control form-control-lg" id="inputPassword" placeholder="password" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center col-md-12">
                                                <button type="submit" class="btn btn-danger px-4 py-2 text-uppercase white font-small-4 box-shadow-2 border-0" id="login" onclick="validateFields();">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include('footer.php'); ?>
        <script>
            function ifLogin(indexPage) {
                if (localStorage.getItem("access_token")) {
                    window.location.href = indexPage;
                }
            }

            $(document).ready(function() {
                ifLogin("study_material.php");
            });

            function validateFields() {
                if ($("#inputUsername").val() === "" || $("#inputPassword").val() === "") {
                    showAlertDialog("username or password cannot be blank..");
                } else {
                    submitDetails();
                }
            }

            function submitDetails() {
                $.ajax({
                    type: "POST",
                    url: BASE_URL + "/coaching/coachinglogin/",
                    dataType: 'json',
                    data: {
                        username: $('#inputUsername').val(),
                        password: $('#inputPassword').val()
                    },
                    async: false,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', "Bearer " + getToken());
                    },
                    success: function(resp) {
                        // showSuccessDialog("Success");
                        // debugger;
                        // flash("Login Successfully");
                        // debugger;
                        saveResponse(resp);
                        window.location.href = "study_material.php";

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        showAlertDialog("Invalid username or password");
                        
                    }
                });
            }

            function saveResponse(resp) {
                localStorage.setItem("access_token", resp.data.token.access_token);
                localStorage.setItem("refresh_token", resp.data.token.refresh_token);
                localStorage.setItem("id", resp.data.info.id);
                localStorage.setItem("name", resp.data.info.name);
                localStorage.setItem("image", resp.data.info.image == null ? '' : resp.data.info.image);
                localStorage.setItem("email", resp.data.info.email);
                localStorage.setItem("mobile", resp.data.info.mobile);
                localStorage.setItem("coachings__name", resp.data.info.coachings[0].coachings__name);
                localStorage.setItem("coachings_id", resp.data.info.coachings[0].coachings_id);
            }
        </script>

</body>

</html>