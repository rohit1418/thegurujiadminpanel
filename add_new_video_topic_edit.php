<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Add New Video Topic</title>

  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="container-fluid">
        <!-- Basic Elements start -->
        <section class="basic-elements">
          <div class="row">
            <div class="col-md-12">
              <h2 class="content-header  btn gradient-blue-grey-blue white shadow-big-navbar">Edit Video Topic</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-title-wrap bar-success">
                    <h4 class="card-title mb-0">Edit Video Topic</h4>
                  </div>
                </div>
                <div class="card-body">
                  <div class="px-3">
                    <form>
                      <div class="form-group row">
                        <div class="col-md-6">
                          <label for="topicTitle">Topic Title</label>
                          <div class="form-label-group">
                            <input type="text" id="topicTitle" class="form-control" placeholder="Topic Title" required="required" autofocus="autofocus">

                          </div>
                        </div>

                        <div class="col-md-6">
                          <label for="topicImageLink">Topic Image</label>
                          <div class="form-label-group">
                            <input type="text" id="topicImageLink" class="form-control" placeholder="Topic Image Link" autofocus="autofocus">

                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-6">
                          <label for="imageFile">Image Upload</label>
                          <div class="input-group">

                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile01" onchange="getTopicImageBrowse();" aria-describedby="inputGroupFileAddon01" name="file" accept="image/*">
                              <label id="filelabel" class="custom-file-label" for="inputGroupFile01" value="Choose File"></label>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <label for="description">Topic Description</label>
                          <textarea class="form-control" id="topicDescription" rows="4" placeholder="Description..."></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-6">
                          <label>Rank</label>
                          <select class="browser-default custom-select mb-4" id="selectTopicRank">
                            <option value="-1" selected>Choose option</option>
                          </select>
                        </div>

                        <div class="col-md-6">
                          <label>Access</label>
                          <div class="form-label-group">
                            <div class="form-check-inline">
                              <label class="form-check-label" for="radio1">
                                <input type="radio" class="form-check-input" id="public" name="status" value="public" checked>Public
                              </label>
                            </div>
                            <div class="form-check-inline">
                              <label class="form-check-label" for="radio2">
                                <input type="radio" class="form-check-input" id="private" name="status" value="private">Private
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <input type="button" class="btn btn-primary btn-block" name="submit" id="update" onClick="validateFields()" value="Update">
                    </form>

                  </div>
                </div>
              </div>

            </div>
          </div>
      </div>
      </section>
      <!-- Sticky Footer -->
    </div>
  </div>
  </div>
  <?php include('footer.php'); ?>

  </div>


  <script>
    var topicImage = null;

    function getTopicImageBrowse() {
      document.getElementById('filelabel').innerHTML = $("#inputGroupFile01").val().substring(12, 80);

      if ($("#inputGroupFile01").val().trim().length > 0) {
        $("#topicImageLink").prop("disabled", true);
        topicImage = $("#inputGroupFile01")[0].files[0];
      } else {
        topicImage = null;
        $("#topicImageLink").prop("disabled", false);
      }
    }


    function getTopicTitle() {

      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/videovacancy/?videovacancymodel_id=" + localStorage.getItem("videoTopicTitle") + "&coaching_id=" + getCoachingId(),
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          $("#topicTitle").val(resp.title);
          $("#topicDescription").val(resp.description);
          $("#selectTopicRank").val(resp.rank);
          $("#" + resp.status).prop("checked", true);
          if (resp.thumbnail_link) {
            $("#filelabel").prop("disabled", true);
            $("#inputGroupFile01").prop("disabled", true);
            $("#topicImageLink").val(resp.thumbnail_link);
            // topicImage = resp.thumbnail_link;
          } else {
            $("#topicImageLink").prop("disabled", true);
            var imageSplit = resp.thumbnail_image.split('/');
            document.getElementById('filelabel').innerHTML = imageSplit[4];

            // topicImage = resp.thumbnail_image;
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log("error " + xhr.responseText)
          showAlertDialog(xhr.responseText.error);
        }
      });
    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }

    $(document).ready(function() {

      ifNotLogin("login.php");

      var topicRanks = "";
      for (let i = 1; i <= 250; i++) {
        topicRanks += "<option value=" + i + ">" + i + "</option>";
      }
      $("#selectTopicRank").append(topicRanks);

      $('#topicImageLink').keyup(function(e) {
        if ($("#topicImageLink").val().trim().length > 0) {
          $("#filelabel").prop("disabled", true);
          $("#inputGroupFile01").prop("disabled", true);
          topicImage = $("#topicImageLink").val().trim();

        } else {
          topicImage = '';
          $("#filelabel").prop("disabled", false);
          $("#inputGroupFile01").prop("disabled", false);
        }
      });

      getTopicTitle();
    });

    function getRank() {
      return $("#selectTopicRank option:selected").val();
    }

    function validateFields() {
      if ($("#topicTitle").val().trim() === "") {
        showAlertDialog("Topic Title can not be blank");

      } else if ($("#topicDescription").val().trim() === "") {
        showAlertDialog("Topic Description can not be blank");

      } else if (getRank() == -1) {
        showAlertDialog('Please choose correct rank');

      } else {
        submitDetails();
      }
    }

    function getStatus() {
      return $('input[name=status]:checked').val();
    }

    function submitDetails() {
      var formData = new FormData();

      if (topicImage != null) {
        if ($("#topicImageLink").val()) {
          formData.append("thumbnail_link", topicImage);
        } else {
          formData.append("thumbnail_image", topicImage);
        }
        formData.append("coaching_id", getCoachingId());
        formData.append("description", $("#topicDescription").val().trim());
        formData.append("title", $("#topicTitle").val().trim());
        formData.append("rank", getRank());
        formData.append("status", getStatus());
        formData.append("videovacancymodel_id", localStorage.getItem("videoTopicTitle"));
      } else {
        formData.append("coaching_id", getCoachingId());
        formData.append("description", $("#topicDescription").val().trim());
        formData.append("title", $("#topicTitle").val().trim());
        formData.append("rank", getRank());
        formData.append("status", getStatus());
        formData.append("videovacancymodel_id", localStorage.getItem("videoTopicTitle"));
      }

      $.ajax({
        type: "PUT",
        url: BASE_URL + "/coaching/videovacancy/",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', 'Bearer ' + getToken());
        },
        success: function(resp) {
          showSuccessDialog('Successfully added new video topic!');
          window.location.href = "video_tutorial.php";
        },
        error: function(xhr, ajaxOptions, thrownError) {
          var data = xhr.responseText;
          var jsonResponse = JSON.parse(data);
          showAlertDialog(jsonResponse.error);
        }
      });
    }
  </script>
  </body>

</html>