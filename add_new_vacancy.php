<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Add New Vacancy</title>

  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12 ">
                <h2 class="content-header btn gradient-blue-grey-blue white shadow-big-navbar">Add new vacancy</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0">Add new vacancy</h4>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="vacancyTitle">Vacancy Title</label>
                            <div class="form-label-group">
                              <input type="text" id="vacancyTitle" class="form-control" placeholder="Vacancy Title" required="required" autofocus="autofocus">

                            </div>
                          </div>

                          <div class="col-md-6">
                            <label for="rank">Rank</label>
                            <div class="form-label-group">
                              <select class="custom-select custom-select-sm" id="selectRank">
                                <option value="-1" selected>Choose Rank</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="form-group row">


                          <div class="col-md-6">
                            <label>Access</label>
                            <div class="form-label-group">
                              <div class="form-check-inline">
                                <label class="form-check-label" for="public">
                                  <input type="radio" class="form-check-input" id="public" name="status" value="public" checked>Public
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="private">
                                  <input type="radio" class="form-check-input" id="private" name="status" value="private">Private
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="summary">Summary</label>
                            <textarea class="form-control" id="summary" rows="4" placeholder="summary..."></textarea>
                          </div>
                        </div>

                        <input type="button" class="btn btn-primary btn-block col-sm-6 offset-sm-3 col-lg-6 offset-lg-3 col-md-6 offset-md-3" id="submit" onClick="validateFields()" value="Submit">
                      </form>

                    </div>
                  </div>
                </div>


                <!-- /.container-fluid -->

                <!-- Sticky Footer -->

              </div>
            </div>
        </div>
        </section>
      </div>
    </div>
  </div>
  </div>
  <?php include('footer.php'); ?>
  <script>
    function getRank() {
      return $("#selectRank option:selected").val();
    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }

    $(document).ready(function() {
      ifNotLogin("login.php");

      var vacancyRanks = "";
      for (let i = 1; i <= 250; i++) {
        vacancyRanks += '<option value=' + i + '>' + i + '</option>';
      }
      $("#selectRank").append(vacancyRanks);
    });

    function validateFields() {
      if ($("#vacancyTitle").val().trim() === "") {
        showAlertDialog("Vacancy Title is cannot be blank");
      } else if ($("#summary").val().trim() === "") {
        showAlertDialog("Summary cannot be blank");
      } else if (getRank() == -1) {
        showAlertDialog('Please choose correct rank');
      } else {
        submitDetails();
      }
    }

    function getStatus() {
      return $('input[name=status]:checked').val();
    }

    function submitDetails() {
      event.preventDefault();
      $.ajax({
        type: "POST",
        url: BASE_URL + "/coaching/vacancy/",
        dataType: 'json',
        data: {
          coaching_id: getCoachingId(),
          rank: getRank(),
          title: $("#vacancyTitle").val().trim(),
          summary: $("#summary").val().trim(),
          status: getStatus()
        },
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          showAlertDialog("New vacancy added!");
          window.location.href = "test_paper.php";
        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText.error);
        }
      });
    }
  </script>

  </body>

</html>