<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Add New Subject</title>


  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12 ">
                <h2 class="content-header btn gradient-blue-grey-blue white shadow-big-navbar">Add new subject</h2>
              </div>
            </div>
            <div class="row">

              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">

                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form method="POST">
                        <div class="form-row">
                          <div class="col-7" style="margin-right: 10px">
                            <input type="text" id="subjectName" class="form-control" placeholder="Enter subject name" required="required" autofocus="autofocus">
                          </div>
                          <div class="col" style="margin-right: 10px">
                            <select class="browser-default custom-select mb-4" id="selectRank">
                              <option value="-1" selected>Choose Rank</option>
                            </select>
                          </div>
                          <div class="col">
                            <button type="submit" class="btn btn-primary btn-block" id="submit" onClick="validateFields()">Save</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                  <!--Get Subject List-->
                  <div class="card mb-3" id="subjectList" style="margin-top: 1rem">
                    <div class="card-header">
                      <div class="pull-left">
                        <h4 class="card-title mb-0">Subejct List</h4>
                      </div>
                      <div class="pull-right">
                        <input type="text" class="searchBar" id="myInput" onkeyup="getSubjectList();" placeholder="Search for names.." title="Type in a name" />
                      </div>
                    </div>

                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-sm" id="dataTable" width="100%" cellspacing="0">
                          <thead class="thead-dark">
                            <tr>
                              <th>S No.</th>
                              <th>Name</th>
                              <th>Rank</th>
                              <th>Date</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody id="tBody"></tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  <!--Confirmation Modal-->


                  </div>
          </div>
      </div>
      </section>
      <!-- Sticky Footer -->
    </div>
  </div>
</div>
<?php include('footer.php'); ?>

</div>

  <div class="modal fade" id="confirm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body">
          Are you sure, you want to delete?
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" id="delete">Delete</button>
          <button type="button" data-dismiss="modal" class="btn btn-sm">Cancel</button>
        </div>
      </div>
    </div>
  </div>



  <script>
    function getRank() {
      return $("#selectRank option:selected").val();
    }

    function validateFields() {
      if ($("#subjectName").val().trim() === "") {
        showAlertDialog("Suject Name is cannot be blank");
      } else if (getRank() == -1) {
        alert('Please choose correct rank');

      } else {
        submitDetails();
      }
    }

    function submitDetails() {
      $.ajax({
        type: "POST",
        url: BASE_URL + "/coaching/subject/",
        dataType: 'json',
        data: {
          coaching_id: getCoachingId(),
          rank: getRank(),
          title: $('#subjectName').val().trim(),
        },
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          showSuccessDialog("New Subject Created!");
          window.location.href = "study_material.php";
        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText + "\n" + xhr.status + "\n" + thrownError);
        }
      });
    }
    localStorage.setItem("subjectId", "0");

    function editItem(el) {
      isActionClicked = true;
      localStorage.setItem("subjectId", el.id);
      window.location.href = "add_new_subject_edit.php"
    }

    function deleteItem(el) {
      isActionClicked = true;
      $('#confirm').modal({
          backdrop: 'static',
          keyboard: false
        })
        .on('click', '#delete', function() {
          $.ajax({
            type: "DELETE",
            url: BASE_URL + "/coaching/subject/?coaching_id=" + getCoachingId() + "&subject_id=" + el.id,
            dataType: 'json',
            data: '{}',
            async: false,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('Authorization', "Bearer " + getToken());
            },
            success: function(resp) {
              getSubjectList();
            },
            error: function(xhr, ajaxOptions, thrownError) {
              showAlertDialog(xhr.responseText.error);
            }
          });
        });
    }

    function getSubjectList() {
      var query = $("#myInput").val();
      if (query === undefined) {
        query = "";
      } else {
        query = query.trim();
      }
      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/subjectslist/?coaching_id=" + getCoachingId() + "&search_type=name" + "&query=" + query,
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          $('#loading-image').hide();
          console.log("token " + getToken());
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          $("#tBody").empty();
          var trHTML = '';
          for (var i = 0; i < resp.length; i++) {
            trHTML +=
              '<tr><td>' +
              (i + 1) +
              '</td><td>' +
              resp[i].name +
              '</td><td>' +
              resp[i].rank +
              '</td><td>' +
              resp[i].created_at__date +
              '</td><td>' +
              '<input type="button" value="Edit" class="btn btn-link" style="color:blue; text-decoration:none;" id="' + resp[i].id + '" onclick="editItem(this);"' + '>' +
              '<input type="button" value="Delete" class="btn btn-link" style="color:red; text-decoration:none;" id="' + resp[i].id + '" onclick="deleteItem(this);"' + '>' +
              '</td></tr>';
          }
          $('#tBody').append(trHTML);
        },
        complete: function() {

        },
        error: function(xhr, ajaxOptions, thrownError) {

          showAlertDialog(xhr.responseText.error);
        }
      });

    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }
    // $('#loading-image').show();
    // $('#subjectList').hide();
    $(document).ready(function() {
      ifNotLogin("login.php");


      var subjectRanks = "";
      for (let i = 1; i <= 250; i++) {
        subjectRanks += '<option value=' + i + '>' + i + '</option>';
      }
      $("#selectRank").append(subjectRanks);


      function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this,
            args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function() {
            callback.apply(context, args);
          }, ms || 0);
        };
      }


      $('#myInput').keyup(delay(function(e) {
        getSubjectList();
      }, 500));
      getSubjectList();
    });
  </script>
  </body>

</html>