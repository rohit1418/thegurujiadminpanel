<!DOCTYPE html>
<html lang="en" class="loading">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Video Tutorial</title>

  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-lg-10 offset-lg-1">
              <div style="display: inline;">
                <div class="card">
                  <!--Get Subject List-->
                  <div class="card-header">
                    <div class="box" style="float: right;">
                      <p class="pstyle">Add new video</p>
                      <p><a href="add_new_video.php" onclick="addNewVideo();" class="divLink" style="text-decoration: none"></a></p>
                    </div>
                    <div class="box ">
                      <p class="pstyle">Add new video topic</p>
                      <p><a href="add_new_video_topic.php" class="divLink" style="text-decoration: none"></a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <section id="grid-option">
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <!--Get Subject List-->
                  <div class="card-header">
                    <div class="pull-left">
                      <h4 class="card-title btn gradient-blue-grey-blue white shadow-big-navbar">Video Topics</h4>
                    </div>
                    <div class="pull-right">
                      <input type="text" class="searchBar" id="myInput" placeholder="Search for names.." title="Type in a name" />
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-sm" id="dataTable" width="100%" cellspacing="0">
                        <thead class="thead-dark">
                          <tr>
                            <th>S No.</th>
                            <th>Topic Title</th>
                            <th>Date</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="tBody"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <!--Confirmation Modal-->

      </div>
    </div>
  </div>
  <!-- Sticky Footer -->
  <?php include('footer.php'); ?>
  </div>
  </div>
  <div class="modal fade" id="confirm">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body">
          Are you sure, you want to delete?
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-primary btn-sm" id="delete">Delete</button>
          <button type="button" data-dismiss="modal" class="btn btn-sm">Cancel</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    var isActionClicked = false;
    localStorage.setItem("videoTopicTitleId", "0");


    function addNewVideo() {
      localStorage.setItem("videoTopicTitleId", "0");
    }

    function showVideoList(el) {
      // debugger;
      if (!isActionClicked) {
        localStorage.setItem("videoTopicTitleId", el.id);
        window.location.href = "add_new_video_list.php";
      }
      isActionClicked = false;
    }

    function editItem(el) {
      isActionClicked = true;
      localStorage.setItem("videoTopicTitle", el.id);
      window.location.href = "add_new_video_topic_edit.php";
    }

    function deleteItem(el) {
      isActionClicked = true;
      $('#confirm').modal({
          backdrop: 'static',
          keyboard: false
        })
        .on('click', '#delete', function() {
          $.ajax({
            type: "DELETE",
            url: BASE_URL + "/coaching/videovacancy/?videovacancymodel_id=" + el.id + "&coaching_id=" + getCoachingId(),
            dataType: 'json',
            data: '{}',
            async: false,
            beforeSend: function(xhr) {
              xhr.setRequestHeader('Authorization', "Bearer " + getToken());
            },
            success: function(resp) {
              getVideoTopicList();
            },
            error: function(xhr, ajaxOptions, thrownError) {
              showAlertDialog(xhr.responseText.error);
            }
          });
        });
    }

    function getVideoTopicList() {
      var query = $("#myInput").val();
      if (query === undefined) {
        query = "";
      } else {
        query = query.trim();
      }

      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/videotopiclist/?coaching_id=" + getCoachingId() + "&search_type=name" + "&query=" + query,
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          $('#loading-image').hide();
          console.log("token " + getToken());
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          $("#tBody").empty();
          var trHTML = '';
          for (var i = 0; i < resp.length; i++) {
            trHTML +=
              '<tr class="table-row" data-href="add_new_video_list.php" id="' + resp[i].id + '" onclick="showVideoList(this);"' + '>' + '<td>' +
              (i + 1) +
              '</td><td>' +
              resp[i].title +
              '</td><td>' +
              resp[i].created_at__date +
              '</td><td>' +
              '<input type="button" value="Edit" class="btn btn-link" style="color:blue; text-decoration:none;" id="' + resp[i].id + '" onclick="editItem(this);"' + '>' +
              '<input type="button" value="Delete" name="remove_levels" data-toggle="modal" data-target="#confirm" class="btn btn-link" style="color:red; text-decoration:none;" id="' + resp[i].id + '" onclick="deleteItem(this);"' + '>' +
              '</td></tr>';
          }
          $('#tBody').append(trHTML);
        },
        complete: function() {
          // $('#loading-image').hide();
          // $('#studyMaterialList').show();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // $('#loading-image').hide();
          // $('#studyMaterialList').show();
          showAlertDialog(xhr.responseText.error);
        }
      });

    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }

    // $('#loading-image').show();
    // $('#studyMaterialList').hide();
    $(document).ready(function() {
      ifNotLogin("login.php");

      function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this,
            args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function() {
            callback.apply(context, args);
          }, ms || 0);
        };
      }


      $('#myInput').keyup(delay(function(e) {
        getVideoTopicList();
      }, 500));
      getVideoTopicList();
    });
  </script>

  </body>

</html>