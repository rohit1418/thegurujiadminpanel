<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Edit Test Paper</title>

  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12 ">
                <h2 class="content-header btn gradient-blue-grey-blue white shadow-big-navbar">Test paper edit</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0">Test paper edit</h4>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form>
                        <div class="form-group row">

                          <div class="col-md-6">
                            <label for="vacancy">Vacancy</label>
                            <select class="custom-select custom-select-sm" id="selectVacancy">
                              <option value="-1" selected>Choose Option</option>
                            </select>
                          </div>
                          <div class="col-md-6">
                            <label for="testPaperTitle">Test Paper Title</label>

                            <input type="text" class="form-control form-control-sm" id="testPaperTitle" placeholder="Test Paper Title">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="time">Time</label>

                            <input type="text" class="form-control form-control-sm" id="time" placeholder="Time in minutes">
                          </div>

                          <div class="col-md-6">

                            <label for="totalQuestions">Total Questions</label>

                            <input type="text" class="form-control form-control-sm" id="totalQuestions" placeholder="Total Questions">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="perQuestionScore">Per Question Score</label>

                            <input type="text" class="form-control form-control-sm" id="perQuestionScore" placeholder="Per Question Score">
                          </div>

                          <div class="col-md-6">
                            <label for="totalMarks">Total Marks</label>

                            <input type="text" class="form-control form-control-sm" id="totalMarks" placeholder="Total Marks">
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="instructions">Instructions</label>

                            <textarea class="form-control form-control-sm" id="instructions" placeholder="instructions..."></textarea>
                          </div>
                          <div class="col-md-3">
                            <label for="startDate">Start Date</label>

                            <input type="text" class="form-control form-control-sm" id="startDate" placeholder="Date">
                          </div>
                          <div class="col-md-3">
                            <label for="endDate">End Date</label>

                            <input type="text" class="form-control form-control-sm" id="endDate" placeholder="Date">
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-6">
                            <label>Access</label>
                            <div class="form-label-group">

                              <div class="form-check-inline">
                                <label class="form-check-label" for="public">
                                  <input type="radio" class="form-check-input" id="public" name="status" value="public" checked>Public
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="private">
                                  <input type="radio" class="form-check-input" id="private" name="status" value="private">Private
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="selectRank">Rank</label>

                            <select class="custom-select custom-select-sm" id="selectRank">
                              <option value="-1" selected>Choose Option</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="questionPaperCode">Question Paper Code</label>
                            <input type="text" class="form-control form-control-sm" id="questionPaperCode" placeholder="Question Paper Code">
                          </div>
                        </div>

                        <div class="col-xmd-6">
                          <input type="submit" class="btn btn-primary" onClick="validateFields()" value="Update" id="submit">
                        </div>
                    </div>

                    </form>
                  </div>
                </div>
              </div>
              <!-- /.container-fluid -->
            </div>
        </div>
      </div>
      </section>
      <!-- Sticky Footer -->
    </div>
  </div>
  </div>
  <?php include('footer.php'); ?>

  </div>

  <script src="app-assets/js/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="app-assets/css/jquery.datetimepicker.min.css"/>
  <script src="app-assets/js/jquery.datetimepicker.js"></script>

  <script>
    var instruction = null;
    function getRank() {
      return $("#selectRank option:selected").val();
    }

    function getStatus() {
      return $('input[name=status]:checked').val();
    }

    function getVacancyTitleId() {
      return $("#selectVacancy option:selected").val();
    }

    function validateFields() {

      var startDate = document.getElementById('startDate').value;
      var endDate = document.getElementById('endDate').value;
      var eDate = new Date(endDate);
      var sDate = new Date(startDate);

      if (getVacancyTitleId() == -1) {
        alert('Please choose correct Vacancy Title');

      } else if ($("#testPaperTitle").val().trim() === "") {
        alert("Test Paper Title can not be blank");

      } else if ($("#time").val().trim() === "") {
        alert("Time can not be blank");

      } else if ($("#totalQuestions").val().trim() === "") {
        alert("Total Question can not be blank");

      } else if ($("#perQuestionScore").val().trim() === "") {
        alert("Per Question Score can not be blank");

      } else if ($("#totalMarks").val().trim() === "") {
        alert("Total Marks can not be blank");

      } else if (startDate == '' || endDate == '') {
        showAlertDialog("Please provide start and end date");

      } else if (startDate != '' && endDate != '' && sDate > eDate) {
        showAlertDialog("Please ensure that the End Date is greater than or equal to the Start Date.");

      } else if (getRank() == -1) {
        alert('Please choose correct rank');

      } else {
        submitDetail();
      }
    }

    function submitDetail() {

      // var sdate = $("#startDate").val();
      // sdateSplit = sdate.split("/");
      // startDate = sdateSplit[2] + "-" + sdateSplit[0] + "-" + sdateSplit[1];

      // var edate = $("#endDate").val();
      // edateSplit = edate.split("/");
      // endDate = edateSplit[2] + "-" + edateSplit[0] + "-" + edateSplit[1];

      if($("#instructions").val().length != 0){
        instruction = $("#instructions").val().trim();
      }else{
        instruction = null;
      }

      var data= {
          name: $("#testPaperTitle").val().trim(),
          terms: instruction,
          no_of_questions: $("#totalQuestions").val().trim(),
          score: $("#totalMarks").val().trim(),
          minutes: $("#time").val().trim(),
          vacancy_id: getVacancyTitleId(),
          rank: getRank(),
          status: getStatus(),
          coaching_id: getCoachingId(),
          expires: $("#endDate").val(),
          start_date: $("#startDate").val(),
          per_question_score: $("#perQuestionScore").val().trim(),
          vacancymodel_id: localStorage.getItem("testPaperTitleId")
      };

      $.ajax({
        type: "PUT",
        url: BASE_URL + "/coaching/vacancymodel/",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          showSuccessDialog('Test paper edited successfully!');
        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText.error);
        }
      });
    }

    function getVacancyList() {
      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/coachingvacancylist/?coaching_id=" + getCoachingId(),
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          var vacancyTitle = "";
          for (let i = 0; i < resp.length; i++) {
            vacancyTitle += '<option value=' + resp[i].id + '>' + resp[i].name + '</option>';
          }
          $("#selectVacancy").append(vacancyTitle);

          if (localStorage.getItem("vacancy_title_id") != "0") {
            $("#selectVacancy").val(localStorage.getItem("vacancy_title_id"));
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(xhr.responseText.error);
        }
      });
    }

    function getTestPaperDetail() {
      debugger;
      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/vacancymodel/?coaching_id=" + getCoachingId() + "&vacancymodel_id=" + localStorage.getItem("testPaperTitleId"),
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          $("#testPaperTitle").val(resp.name);
          $("#time").val(resp.minutes);
          $("#totalQuestions").val(resp.no_of_questions);
          $("#perQuestionScore").val(resp.per_question_score);
          $("#totalMarks").val(resp.score);
          $("#instructions").val(resp.terms);
          var endDate = resp.expires;
          endDate = endDate.replace('Z', '').replace('T', ' ');
          $("#endDate").val(endDate);
          var startDate = resp.start_date;
          startDate = startDate.replace('Z', '').replace('T', ' ');
          $("#startDate").val(startDate);
          $("#selectRank").val(resp.rank);
          $("#questionPaperCode").val(resp.paper_code);
          $("#" + resp.status).prop("checked", true);
        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText.error);
        }
      });

    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }

    $(document).ready(function() {

      ifNotLogin("login.php");

      var vacancyRanks = "";
      for (let i = 1; i <= 250; i++) {
        vacancyRanks += '<option value=' + i + '>' + i + '</option>';
      }
      $("#selectRank").append(vacancyRanks);

      getVacancyList();
      getTestPaperDetail();
    });

    $("#startDate").datetimepicker({
		format:'Y-m-d H:i:s'  
	  });
      $("#endDate").datetimepicker({
		format:'Y-m-d H:i:s'    
    });

  </script>

  </body>

</html>