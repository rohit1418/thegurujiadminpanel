<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Add New Video Topic</title>
  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12">
                <h2 class="content-header  btn gradient-blue-grey-blue white shadow-big-navbar">Add New Video Topic</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0">Add New Video Topic</h4>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="topicTitle">Topic Title</label>
                            <div class="form-label-group">
                              <input type="text" id="topicTitle" class="form-control" placeholder="Topic Title" required="required" autofocus="autofocus">

                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="topicImageLink">Topic Image Link</label>
                            <div class="form-label-group">
                              <input type="text" id="topicImageLink" class="form-control" placeholder="Topic Image Link" autofocus="autofocus">

                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="pdfLink">Image Upload</label>
                            <div class="input-group">

                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" onchange="getTopicImageBrowse();" aria-describedby="inputGroupFileAddon01" name="file" accept="image/*">
                                <label id="filelabel" class="custom-file-label" for="inputGroupFile01" value="Choose File"></label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label>Rank</label>
                            <select class="browser-default custom-select mb-4" id="selectTopicRank">
                              <option value="-1" selected>Choose option</option>
                            </select>
                          </div>


                        </div>
                        <div class="form-group row">

                          <div class="col-md-6">
                            <label>Access</label>
                            <div class="form-label-group">
                              <div class="form-check-inline">
                                <label class="form-check-label" for="radio1">
                                  <input type="radio" class="form-check-input" id="public" name="status" value="public" checked>Public
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="radio2">
                                  <input type="radio" class="form-check-input" id="private" name="status" value="private">Private
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="description">Topic Description</label>
                            <textarea class="form-control" id="topicDescription" rows="4" placeholder="Description..."></textarea>
                          </div>
                        </div>
                        <input type="button" class="btn btn-primary btn-block col-sm-6 offset-sm-3 col-lg-6 offset-lg-3 col-md-6 offset-md-3" name="submit" id="submit" onClick="validateFields()" value="Submit">
                      </form>

                    </div>
                  </div>
                </div>


                <!-- /.container-fluid -->
              </div>
            </div>
        </div>
        </section>
      </div>
    </div>
  </div>
  <!-- Sticky Footer -->
  <?php include('footer.php'); ?>
  </div>

  <script>
    var topicImage = '';

    function getTopicImageBrowse() {
      document.getElementById('filelabel').innerHTML = $("#inputGroupFile01").val().substring(12, 80);

      if ($("#inputGroupFile01").val().trim().length > 0) {
        $("#topicImageLink").prop("disabled", true);
        topicImage = $("#inputGroupFile01")[0].files[0];
      } else {
        topicImage = '';
        $("#topicImageLink").prop("disabled", false);
      }
    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }
    $(document).ready(function() {

      ifNotLogin("login.php");

      var topicRanks = "";
      for (let i = 1; i <= 250; i++) {
        topicRanks += '<option value=' + i + '>' + i + '</option>';
      }
      $("#selectTopicRank").append(topicRanks);

      $('#topicImageLink').keyup(function(e) {
        if ($("#topicImageLink").val().trim().length > 0) {
          $("#filelabel").prop("disabled", true);
          $("#inputGroupFile01").prop("disabled", true);
          topicImage = $("#topicImageLink").val().trim();

        } else {
          topicImage = '';
          $("#filelabel").prop("disabled", false);
          $("#inputGroupFile01").prop("disabled", false);
        }
      });

    });

    function getRank() {
      return $("#selectTopicRank option:selected").val();
    }

    function validateFields() {
      debugger;
      if ($("#topicTitle").val().trim() === "") {
        showAlertDialog("Topic Title can not be blank");

      } else if ($("#topicDescription").val().trim() === "") {
        showAlertDialog("Topic Description can not be blank");

      } else if (getRank() == -1) {
        showAlertDialog('Please choose correct rank');

      } else if (topicImage.length == 0) {
        showAlertDialog("Please provide topic image or link");

      } else {
        submitDetails();
      }
    }

    function getStatus() {
      return $('input[name=status]:checked').val();
    }

    function submitDetails() {
      var formData = new FormData();
      if($("#topicImageLink").val()){
        formData.append("thumbnail_link", topicImage);
      }else{
        formData.append("thumbnail_image", topicImage);
      }
      console.log(topicImage);

      formData.append("coaching_id", getCoachingId());
      formData.append("description", $("#topicDescription").val().trim());
      // formData.append("thumbnail_image", topicImage);
      formData.append("title", $("#topicTitle").val().trim());
      formData.append("rank", getRank());
      formData.append("status", getStatus());

      $.ajax({
        type: "POST",
        url: BASE_URL + "/coaching/videovacancy/",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', 'Bearer ' + getToken());
        },
        success: function(resp) {
          showSuccessDialog('Successfully added new video topic!');
          window.location.href = "video_tutorial.php";
        },
        error: function(xhr, ajaxOptions, thrownError) {
          var data = xhr.responseText;
          var jsonResponse = JSON.parse(data);
          showAlertDialogDialog(jsonResponse.error);
        }
      });
    }
  </script>
  </body>

</html>