<body id="page-top">


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!--Helper js file-->
  <script src="helperFiles/helper.js"></script>

  <!--Toaster js file-->
  <script src="toaster/flash.js"></script>
  <script src="toaster/flash.min.js"></script>


<script>
    $(document).ready(function () {
        if(localStorage.getItem("image")){
          var image=localStorage.getItem("image");
          $("#userDropdown").append('<div style="display: inline-block; width: 90px">'+'<h7 style="margin-right: 15px; color: white">'+localStorage.getItem("name")+'</h7>'+'<img class="img-circle" src="'+image+'" alt="logo" style="border-radius: 50%; width: 30px; height: 30px;" >'+'</div>');

        }else{
          // $("#userDropdown").append('<i class="fa fa-user-circle fa-fw"></i>');
          $("#userDropdown").append('<div style="display: inline-block; width: 90px">'+'<h7 style="margin-right: 15px; color: white">'+localStorage.getItem("name")+'</h7>'+'<img class="img-circle" src="images/font-awesome.png" alt="logo" style="border-radius: 50%; width: 30px; height: 30px;" >'+'</div>');
        }

    });
</script>

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.php">theguruji admin panel</a>

    <!-- <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button> -->


    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-6">
      
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <!-- <i class="fas fa-user-circle fa-fw"></i> -->
          <!-- <img class="img-circle" src="images/github.png" alt="logo" style="border-radius: 50%; width: 25px; height: 30px;"> -->
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <!-- <a class="dropdown-item" href="#">Settings</a>
          <a class="dropdown-item" href="#">Activity Log</a> -->
          <!-- <div class="dropdown-divider"></div> -->
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        <!-- </div> -->
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
        <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-fw fa-folder"></i>
              <span>Pages</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
              <a class="dropdown-item" href="login.php">Login</a>
              <a class="dropdown-item" href="register.php">Register</a> -->
              <!-- <a class="dropdown-item" href="forgot-password.php">Forgot Password</a> -->
          <!-- </li> -->
          
        <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-folder"></i>
                <span>Content</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <a class="dropdown-item" href="study_material.php">Study Material</a>
                <a class="dropdown-item" href="video_tutorial.php">Video Tutorial</a>
                <a class="dropdown-item" href="test_paper.php">Test Paper</a>
      
            </li>
      
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-folder"></i>
                <span>Report</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <a class="dropdown-item" href="paper_wise_report.php">Paper Wise Report</a>
                <a class="dropdown-item" href="student_wise_report.php">Student Wise Report</a>
      
            </li>
        </ul>

