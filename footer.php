  <footer class="footer footer-static footer-light">
    <p class="clearfix text-muted text-center px-2"><span>Copyright &copy; 2018 The guruji Admin Panel , All rights reserved. </span></p>
  </footer>

  </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Are you sure you want to logout?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.php" onclick="logout();">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <script>
    function logout() {
      localStorage.clear();
    }
  </script>
  <script src="helperFiles/helper.js"></script>
  <!-- BEGIN VENDOR JS-->
  <script src="app-assets/vendors/js/core/jquery-3.3.1.min.js"></script>
  <script src="app-assets/vendors/js/core/popper.min.js"></script>
  <script src="app-assets/vendors/js/core/bootstrap.min.js"></script>
  <script src="app-assets/vendors/js/perfect-scrollbar.jquery.min.js"></script>
  <script src="app-assets/vendors/js/prism.min.js"></script>
  <script src="app-assets/vendors/js/jquery.matchHeight-min.js"></script>
  <script src="app-assets/vendors/js/screenfull.min.js"></script>
  <script src="app-assets/vendors/js/pace/pace.min.js"></script>
  <script src="app-assets/vendors/js/datatable/datatables.min.js"></script>
  <!-- <script src="app-assets/js/data-tables/datatables-sources.js"></script>-->
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN CONVEX JS-->
  <script src="app-assets/js/app-sidebar.js"></script>
  <script src="app-assets/js/notification-sidebar.js"></script>
  <script src="app-assets/js/customizer.js"></script>
  <!-- END CONVEX JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->

  <script>
    $(document).ready(function() {
      if (localStorage.getItem("image")) {
        var image = localStorage.getItem("image");
        $("#dropdownBasic3").append('<div style="display: inline-block; width: 60px">' + '<h7 style="margin-right: 15px; color: white">' + localStorage.getItem("name") + '</h7>' + '</div>');

      } else {
        $("#dropdownBasic3").append('<div style="display: inline-block; width: 60px">' + '<h7 style="margin-right: 15px; color: white">' + localStorage.getItem("name") + '</h7>' + '</div>');
      }

    });
  </script>