<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Study Material Edit</title>


  <?php include('header.php'); ?>
  <div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <div class="container-fluid">
          <!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-md-12 ">
                <h2 class="content-header btn gradient-blue-grey-blue white shadow-big-navbar">Study material edit</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 ">
                <div class="card">
                  <div class="card-header">
                    <div class="card-title-wrap bar-success">
                      <h4 class="card-title mb-0">Study material edit</h4>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="px-3">
                      <form>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="selectSubject">Subject</label>
                            <div class="form-label-group">
                              <input type="text" id="selectSubject" class="form-control" placeholder="Subject" required="required" autofocus="autofocus">

                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="pdfTitle">PDF Title</label>
                            <div class="form-label-group">
                              <input type="text" id="pdfTitle" class="form-control" placeholder="PDF Title" required="required" autofocus="autofocus">

                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="inputGroupFile01">PDF Upload</label>
                            <div class="input-group">
                              &nbsp;
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="file" onchange="getPdfBrowse();" accept="application/pdf">
                                <label id="filelabel" class="custom-file-label" for="inputGroupFile01" value="Choose File"></label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="pdfLink">PDF Link</label>
                            <div class="form-label-group">
                              <input type="text" id="pdfLink" class="form-control" placeholder="PDF Link" required="required" autofocus="autofocus">

                            </div>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="col-md-6">
                            <label>Rank</label>
                            <select class="browser-default custom-select mb-4" id="selectRank">
                              <option value="-1" selected>Choose option</option>
                            </select>
                          </div>

                          <div class="col-md-6">
                            <label>Access</label>
                            <div class="form-label-group">
                              <div class="form-check-inline">
                                <label class="form-check-label" for="public">
                                  <input type="radio" class="form-check-input" id="public" name="status" value="public" checked>Public
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="private">
                                  <input type="radio" class="form-check-input" id="private" name="status" value="private">Private
                                </label>

                              </div>
                            </div>
                          </div>
                        </div>

                        <input type="button" class="btn btn-primary btn-block col-sm-6 offset-sm-3 col-lg-6 offset-lg-3 col-md-6 offset-md-3" id="update" onClick="validateFields();" value="Update">

                    </div>
                  </div>
                </div>


              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

  <!-- Sticky Footer -->
  <?php include('footer.php'); ?>

  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>

  <script>
    var subjectId = "";
    var pdfTopic = null;
    var file_type = null;

    function getStatus() {
      return $("input[name='status']:checked").val();
    }

    function getRank() {
      return $("#selectRank option:selected").val();
    }

    function validateFields() {
      if ($("#selectSubject").val() === "") {
        showAlertDialog("Subject field cannot be blank");
      } else if ($("#pdfTitle").val().trim() === "") {
        showAlertDialog("PDF title field cannot be blank");
      } else if (getRank() == -1) {
        showAlertDialog("Please select correct rank");
      } else {
        submitFields();
      }
    }


    function getPdfBrowse() {
      document.getElementById('filelabel').innerHTML = $("#inputGroupFile01").val().substring(12, 80);
      if ($("#inputGroupFile01").val().trim().length > 0) {
        $("#pdfLink").prop("disabled", true);
        pdfTopic = $("#inputGroupFile01").val().trim();

        file_type = (function(pdfTopic, lio) {
          return lio === -1 ? undefined : pdfTopic.substring(lio + 1);
        })(pdfTopic, pdfTopic.lastIndexOf("."));

      } else {
        pdfTopic = null;
        $("#pdfLink").prop("disabled", false);
      }
    }

    function ifNotLogin(loginPage) {
      if (!localStorage.getItem("access_token")) {
        window.location.href = loginPage;
      }
    }
    $(document).ready(function() {
      ifNotLogin("login.php");

      var subjectRanks = "";
      for (let i = 1; i <= 250; i++) {
        subjectRanks += '<option value=' + i + '>' + i + '</option>';
      }
      $("#selectRank").append(subjectRanks);

      $('#pdfLink').keyup(function(e) {
        if ($("#pdfLink").val().trim().length > 0) {
          $("#filelabel").prop("disabled", true);
          $("#inputGroupFile01").prop("disabled", true);
          pdfTopic = $("#pdfLink").val().trim();
          file_type = pdfTopic.substring(pdfTopic.lastIndexOf('.') + 1, pdfTopic.length) || pdfTopic;

        } else {
          pdfTopic = null;
          $("#filelabel").prop("disabled", false);
          $("#inputGroupFile01").prop("disabled", false);
        }
      });

      $.ajax({
        type: "GET",
        url: BASE_URL + "/coaching/studymaterial/?study_material_id=" + localStorage.getItem("studyMaterialId") + "&coaching_id=" + getCoachingId(),
        dataType: 'json',
        data: '{}',
        async: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', "Bearer " + getToken());
        },
        success: function(resp) {
          subjectId = resp.subject_id;
          $("#pdfTitle").val(resp.title);
          $("#pdfLink").val(resp.pdf_link);
          $("#selectSubject").val(resp.subject_name);
          $("#selectRank").val(resp.rank);
          $("#" + resp.status).prop("checked", true);
          if (resp.pdf_link) {
            $("#filelabel").prop("disabled", true);
            $("#inputGroupFile01").prop("disabled", true);
            $("#pdfLink").val(resp.pdf_link);
            // pdfTopic = resp.pdf_link;
          } else {
            $("#pdfLink").prop("disabled", true);
            var pdfSplit = resp.pdf_file.split('/');
            document.getElementById('filelabel').innerHTML = pdfSplit[4];


          }


        },
        error: function(xhr, ajaxOptions, thrownError) {
          showAlertDialog(xhr.responseText.error);
        }
      });

    });

    function submitFields() {
      var formData = new FormData();

      if (pdfTopic != null) {
        if ($("#pdfLink").val()) {
          formData.append("pdf_link", pdfTopic);

        } else {
          formData.append("pdf_file", $("#inputGroupFile01")[0].files[0]);

        }
        formData.append("coaching_id", getCoachingId());
        formData.append("subject_id", subjectId);
        formData.append("title", $("#pdfTitle").val().trim());
        formData.append("file_type", file_type);
        formData.append("rank", getRank());
        formData.append("status", getStatus());
        formData.append("subject_material_id", localStorage.getItem("studyMaterialId"));
      } else {
        formData.append("coaching_id", getCoachingId());
        formData.append("subject_id", subjectId);
        formData.append("title", $("#pdfTitle").val().trim());
        formData.append("rank", getRank());
        formData.append("status", getStatus());
        formData.append("subject_material_id", localStorage.getItem("studyMaterialId"));
      }

      $.ajax({
        type: "PUT",
        url: BASE_URL + "/coaching/studymaterial/",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('Authorization', 'Bearer ' + getToken());
        },
        success: function(resp) {
          showSuccessDialog('Successfully edited study material!');
          window.location.href = "study_material.php";
        },
        error: function(xhr, ajaxOptions, thrownError) {
          var data = xhr.responseText;
          var jsonResponse = JSON.parse(data);
          debugger;
          showAlertDialog(jsonResponse.error);
        }
      });
    }
  </script>

  </body>

</html>